/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx,.module.js}'],
  theme: {
    extend: {
      colors: {
        transparent: 'transparent',
        current: 'currentColor',
        'light-purple': 'rgb(147 112 219 / 10%)'
      },
    },

    fontSize: {
      sm: '0.8rem',
      base: '1rem',
      xl: '65px',
      '2xl': '1.563rem',
    },

    container: {
      center: true,
      padding: '2rem',
      maxWidth: '1720px',
    },
  },
  plugins: [],
}
