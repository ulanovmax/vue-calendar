export function loadLocations() {
    // Locations Api
    return fetch('https://countriesnow.space/api/v0.1/countries').then((response) =>
        response.json()
    );
}
