import { createApp } from 'vue';

import App from './App.vue';

import './assets/tailwind.css';

import './assets/styles/style.scss';

import 'pickerjs/dist/picker.css'

createApp(App).mount('#app')
